//WAP to find the volume of a tromboloid using 4 functions.

#include<stdio.h>

float input()
{
  float x;
  scanf("%f",&x);
  return x;
}

float volume(float p,float q,float r)

{
  float vol;
  vol=((p*r)+r)/(3*q);
  return vol;
}

float output(float h,float b,float d,float v)

{
  printf("the dimensions of the tromboloid are height=%f, breadth=%f and depth=%f\n",h,b,d);
  printf("the volume of tromboloid is: %f",v);
  
}

int main()

{
  printf("enter the height breadth and depth of tromboloid respectively\n");
  float h,b,d,v;
  h=input();
  b=input();
  d=input();
  v=volume(h,b,d);
  output(h,b,d,v);
  return 0;
}
