//WAP to find the distance between two point using 4 functions.

#include<stdio.h>
#include<math.h>
float input()
{
float a;
scanf("%f",&a);
return a;
}


float distance(float x1,float y1,float x2,float y2)
{
float distance;
distance=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
return distance;
}

float output(float x1,float y1,float x2,float y2,float z)
{
printf("distance between the point (%f,%f) and (%f,%f) is: %f\n",x1,y1,x2,y2,z);
}


int main()
{
printf("enter the abscissa and the ordinate of point-1 and point-2 respectively\n");
float x1,x2,y1,y2,z;
x1=input();
x2=input();
y1=input();
y2=input();
z=distance(x1,x2,y1,y2);
output(x1,y1,x2,y2,z);
return 0;
}