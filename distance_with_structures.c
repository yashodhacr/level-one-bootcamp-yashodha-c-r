//WAP to find the distance between two points using structures and 4 functions.

#include<stdio.h>
#include<math.h>
struct point
{
    float x;
    float y;
};
typedef struct point P;

P input()
{
    P p;
    printf("enter (x, y)\n");
    scanf("%f%f",&p.x,&p.y);
    return p;
}
float distance(P p1, P p2)
{
    float distance;
    distance = sqrt((pow((p1.x-p2.x), 2)) + pow((p1.y-p2.y), 2));
    return distance;
}
void output(P p1, P p2, float dist)
{
    printf("the distance between %.2f,%.2f and %.2f,%.2f is %.2f", p1.x, p1.y, p2.x, p2.y, dist);

}
int main(void)
{
    float dist;
    P p1, p2;
    p1 = input();
    p2 = input();
    dist = distance(p1, p2);
    output(p1, p2, dist);
    return 0;
}
